object mfmWizardTester: TmfmWizardTester
  Left = 0
  Top = 0
  Caption = 'DUnitX Wizard Tester'
  ClientHeight = 59
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnLaunchWizard: TButton
    Left = 104
    Top = 8
    Width = 105
    Height = 25
    Caption = 'Launch wizard'
    TabOrder = 0
    OnClick = btnLaunchWizardClick
  end
end
