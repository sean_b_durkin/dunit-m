unit DUnitM.LaunchWizardForm;
interface
uses DUnitM.WizardPageIntf, SBD.ServiceProvider, DUnitM.IDE_API;


type
TTreeViewChoice = (tvDUnitMVTree,    // The DUnitM bundled TVirtualStringTree
                   tvUserVTree,      // The users's pre-installed TVirtualStringTree
                   tvWinTreeView);   // Windows TTreeView

TPlatform = (pWin32, pWin64);
TViewParadigm = (vMVVM_GUI, vConsole);


///<summary>
///  Launch the form wizard and enquire parameters from the user.
///  For parameter definitions see DUnitM.WizardStates
///</summary>
function LaunchWizardForm(
    var   isNewProjectGroup: boolean;      // In and out
    const CurrentProjectName: string;      // In
    const CurrentProjectPath: string;      // In
    var   CurrentPlatform: TPlatform;      // In and out
    const BDSProjectPath: string;          // In
    var   LibraryAbsolutePath: string;     // In and out
    var   UnitTestingProjectName: string;  // Out
    var   UnitTestingLocation: string;     // Out
    var   ApplicationTitle: string;        // Out
    var   Tree: TTreeViewChoice;           // Out
    var   View: TViewParadigm;             // Out
    var   LibraryRelativePath: string;     // Out
    const IDE1: IIDE_API)
    : boolean;  // True if accepted. False if user cancelled.

type
ILaunchWizardFormService = interface
  ['{2D69AF4A-8B28-496E-8FA4-E7161285E73F}']
    function LaunchWizardForm(
        var   isNewProjectGroup: boolean;      // In and out
        const CurrentProjectName: string;      // In
        const CurrentProjectPath: string;      // In
        var   CurrentPlatform: TPlatform;      // In and out
        const BDSProjectPath: string;          // In
        var   LibraryAbsolutePath: string;     // In and out
        var   UnitTestingProjectName: string;  // Out
        var   UnitTestingLocation: string;     // Out
        var   ApplicationTitle: string;        // Out
        var   Tree: TTreeViewChoice;           // Out
        var   View: TViewParadigm;             // Out
        var   LibraryRelativePath: string;     // Out
        const IDE1: IIDE_API)
        : boolean;  // True if accepted. False if user cancelled.
  end;


procedure RegisterStockLaunchWizardService( const ServiceContainer: IServiceProvider);

implementation












uses
 {$if CompilerVersion >= 23}
    // XE2+
    Vcl.Controls,
  {$else}
    // D2010, XE
    Controls,
  {$ifend}
    DUnitM.fmProjectWizard, SBD.Generics, StrUtils,
    DUnitM.frmMainDetailsPage, SysUtils, DUnitM.frmPathPage,
    DUnitM.frmMultipleChoicePage, DUnitM.frmReadMemoPage,
    DUnitM.WizardStates;


type
TDUnitMWizardPageGang = class( TList2<IWizardPageFactory>, IWizardPageFactoryGang);

TStockLauncher = class( TInterfacedObject, ILaunchWizardFormService)
  private
    function LaunchWizardForm(
        var   isNewProjectGroup: boolean;
        const CurrentProjectName: string;
        const CurrentProjectPath: string;
        var   CurrentPlatform: TPlatform;
        const BDSProjectPath: string;
        var   LibraryAbsolutePath: string;
        var   UnitTestingProjectName: string;
        var   UnitTestingLocation: string;
        var   ApplicationTitle: string;
        var   Tree: TTreeViewChoice;
        var   View: TViewParadigm;
        var   LibraryRelativePath: string;
        const IDE1: IIDE_API)
        : boolean;
  end;

procedure RegisterStockLaunchWizardService( const ServiceContainer: IServiceProvider);
begin
if assigned( ServiceContainer) then
  ServiceContainer.RegisterFlyweightService( ILaunchWizardFormService,
    function (const Config: string; const ServiceProvider: IServiceProvider): IInterface
    begin
    result := TStockLauncher.Create as ILaunchWizardFormService
    end)
end;


function BoolToString( doCreateNewProjectGroup: boolean): string;
begin
result := IfThen( doCreateNewProjectGroup, 'yes', 'no')
end;

function TreeViewChoiceToString( Tree: TTreeViewChoice): string;
const TreeStrs: array[ TTreeViewChoice] of string = (
  'The DUnitM bundled TVirtualStringTree',
  'Your pre-installed TVirtualStringTree',
  'Windows TTreeView');
begin
result := TreeStrs[ Tree]
end;


function PlatformToString( Platform1: TPlatform): string;
const PlatformStrs: array[ TPlatform] of string = (
  'Win32',
  'Win64');
begin
result := PlatformStrs[ Platform1]
end;


function ViewToString( View: TViewParadigm): string;
const ParadigmStrs: array[ TViewParadigm] of string = (
  'MV-V-M GUI',
  'Console');
begin
result := ParadigmStrs[ View]
end;



function LaunchWizardForm(
    var   isNewProjectGroup: boolean;
    const CurrentProjectName: string;
    const CurrentProjectPath: string;
    var   CurrentPlatform: TPlatform;
    const BDSProjectPath: string;
    var   LibraryAbsolutePath: string;
    var   UnitTestingProjectName: string;
    var   UnitTestingLocation: string;
    var   ApplicationTitle: string;
    var   Tree: TTreeViewChoice;
    var   View: TViewParadigm;
    var   LibraryRelativePath: string;
    const IDE1: IIDE_API)
    : boolean;
var
  Environment: RWizardInitialState;
  UserData: RWizardFinalState;
  Wiz: TfmProjectWizard;
  Services: IServiceProvider;
begin
Environment.Create( isNewProjectGroup, CurrentProjectName, CurrentProjectPath,
                    CurrentPlatform, BDSProjectPath, LibraryAbsolutePath);
Wiz := TfmProjectWizard.Create( nil);
Services := StandardServiceProvider;
try
  Services.RegisterFlyweightService( IWizardPageFactoryGang,
    function (const Config: string; const ServiceProvider: IServiceProvider): IInterface
    var
      Gang: IWizardPageFactoryGang;
    begin
      // Page 1
      Gang := TDUnitMWizardPageGang.Create as IWizardPageFactoryGang;
      result := Gang;
      Gang.Add( TfrmMainDetailsPage.TFactory.Create( IDE1));

      // Page 2 -- Suppress page 2 until we develop alternative tree controls.
      Gang.Add( TfrmMultipleChoice.TFactory.Create(
        {PageId = } 'tree',
        {NextId= } ComputeNextPage('tree'),
        {Title= } 'Select the tree view class',
        {InstructionText= } 'You can only select the pre-installed version of TVirtualStringTree if you'#13#10 +
                            'already have a copy installed. For more information see http://www.soft-gems.net',
        {GroupCaption= } 'Tree class',
        {Items= } '|The DUnitM bundled TVirtualStringTree' +
                  '|Your pre-installed TVirtualStringTree' +
                  '|Boring old TTreeView',
        {InitItemSelectionProc = } procedure( const PassInState: IInterface; var SelectedItem: integer)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              SelectedItem := Ord( State.State.FFinl.FTree)
          end,

        {ParseItemSelectionProc= } procedure( const PassInState: IInterface; SelectedItem: integer; var PassOutState: IInterface)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              begin
              State.SetTree( TTreeViewChoice( SelectedItem));
              PassOutState := State.Clone
              end
            else
              PassOutState := nil
          end
        ));

      // Page 3
      Gang.Add( TfrmMultipleChoice.TFactory.Create(
        {PageId = } 'platform',
        {NextId= } ComputeNextPage('platform'),
        {Title= } 'Select the initial project platform',
        {InstructionText= } 'OS X, iOS and Android are not yet supported.',
        {GroupCaption= } 'Target platform',
        {Items= } '|Win32' +
                  '|Win64',
        {InitItemSelectionProc = } procedure( const PassInState: IInterface; var SelectedItem: integer)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              SelectedItem := Ord( State.State.FFinl.FPlatform)
          end,

        {ParseItemSelectionProc= } procedure( const PassInState: IInterface; SelectedItem: integer; var PassOutState: IInterface)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              begin
              State.SetPlatform( TPlatform( SelectedItem));
              PassOutState := State.Clone
              end
            else
              PassOutState := nil
          end
        ));

      // Page 4
      Gang.Add( TfrmMultipleChoice.TFactory.Create(
        {PageId = } 'paradigm',
        {NextId= } ComputeNextPage('paradigm'),
        {Title= } 'Select the view paradigm',
        {InstructionText= } 'This page is a place-marker-only.'#13#10 +
                            'At the moment, we only support Model-View View Model GUI paradigm.',
        {GroupCaption= } 'View paradigm',
        {Items= } '|MVVM GUI' +
                  '|Console',
        {InitItemSelectionProc = } procedure( const PassInState: IInterface; var SelectedItem: integer)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              SelectedItem := Ord( State.State.FFinl.FView)
          end,

        {ParseItemSelectionProc= } procedure( const PassInState: IInterface; SelectedItem: integer; var PassOutState: IInterface)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              begin
              State.SetView( TViewParadigm( SelectedItem));
              PassOutState := State.Clone
              end
            else
              PassOutState := nil
          end
        ));

      // Page 5
      Gang.Add( TfrmPathPage.TFactory.Create( IDE1));

      // Page 6: ReadMe (about what to do next)
      Gang.Add( TfrmReadMemo.TFactory.Create( 'read-me', 'summary', procedure( const PassInState: IInterface; var MemoText: string)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
              begin
              MemoText :=
'Before you can run your Unit Testing Runner program, you will need to ...'#13#10 +
'  (1) Write your unit tests;'#13#10 +
'  (2) Write code for factories (ILoggerContainerFactory) for any secondary logger instances that you might want to attach to your runner. This is an optional step.'#13#10 +
'  (3) Edit unit DUnitM.Executive.pas in two places ...'#13#10 +
'    (3.1) Within method TExecutive.RegisterTestFixtures(), register your test fixtures as per comments.'#13#10 +
'    (3.2) Within method TExecutive.RegisterServices(), acquire the ILoggerCentralRegister instance and add your secondary logger factories to this list.';
              end
          end, IDE1));

      // Page 7: Summary of all options
      Gang.Add( TfrmReadMemo.TFactory.Create( 'summary', '', procedure( const PassInState: IInterface; var MemoText: string)
          var
            State: IWizardIntermediateState;
          begin
          if Supports( PassInState, IWizardIntermediateState, State) then
            with State.FinalState.Data do
              MemoText := Format(
                'Summary'#13#10 +
                '--------'#13#10 +
                #13#10 +
                'A DUnitM unit testing application will be generated with the following parameters:'#13#10 +
                'Create project in new project group = %0:s'#13#10 + // %0:s == FdoCreateNewProjectGroup
                'Project name = %1:s'#13#10 +                        // %1:s == FUnitTestingProjectName
                'Project location = %2:s'#13#10 +                    // %2:s == FUnitTestingLocation
                'Application title = %3:s'#13#10 +                   // %3:s = FApplicationTitle
                'Tree class = %4:s'#13#10 +                          // %4:s == FTree
                'Platform = %5:s'#13#10 +                            // %5:s == FPlatform
                'View paradigm = %6:s'#13#10 +                       // %6:s == FView
                'DUnitM library path (absolute) = %7:s'#13#10 +      // %7:s == FLibraryAbsolutePath
                'DUnitM library path (relative) = %8:s'#13#10,       // %8:s == FLibraryRelativePath
                [
                 {%0:s} BoolToString( FdoCreateNewProjectGroup),
                 {%1:s} FUnitTestingProjectName,
                 {%2:s} FUnitTestingLocation,
                 {%3:s} FApplicationTitle,
                 {%4:s} TreeViewChoiceToString( FTree),
                 {%5:s} PlatformToString( FPlatform),
                 {%6:s} ViewToString( FView),
                 {%7:s} FLibraryAbsolutePath,
                 {%8:s} FLibraryRelativePath
                ])
          end, IDE1));
    end,
    '');
  Wiz.FServices  := Services;
  Wiz.FStartPage := ComputeNextPage('');
  Wiz.FEnvironment := CreateWizardInitialState( Environment);
  result := Wiz.ShowModal = mrOk;
  if result then
    UserData := Wiz.FUserData.Data
finally
  Services := nil;
  Wiz.Free
  end;
if result then
  begin
  isNewProjectGroup      := UserData.FdoCreateNewProjectGroup;
  CurrentPlatform        := UserData.FPlatform;
  LibraryAbsolutePath    := UserData.FLibraryAbsolutePath;
  UnitTestingProjectName := UserData.FUnitTestingProjectName;
  UnitTestingLocation    := UserData.FUnitTestingLocation;
  ApplicationTitle       := UserData.FApplicationTitle;
  Tree                   := UserData.FTree;
  View                   := UserData.FView;
  LibraryRelativePath    := UserData.FLibraryRelativePath
  end
end;


function TStockLauncher.LaunchWizardForm(var isNewProjectGroup: boolean;
  const CurrentProjectName, CurrentProjectPath: string;
  var CurrentPlatform: TPlatform; const BDSProjectPath: string;
  var LibraryAbsolutePath, UnitTestingProjectName, UnitTestingLocation,
  ApplicationTitle: string; var Tree: TTreeViewChoice; var View: TViewParadigm;
  var LibraryRelativePath: string;
  const IDE1: IIDE_API): boolean;
begin
// The stock launcher is stateless and just defaults to the static function.
result := DUnitM.LaunchWizardForm.LaunchWizardForm(
  isNewProjectGroup, CurrentProjectName, CurrentProjectPath,
  CurrentPlatform, BDSProjectPath, LibraryAbsolutePath, UnitTestingProjectName,
  UnitTestingLocation, ApplicationTitle, Tree, View, LibraryRelativePath, IDE1)
end;

end.
