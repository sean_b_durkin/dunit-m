unit DUnitM.DesignReg;
interface

procedure Register;

implementation











uses
  DUnitM.Wizard, DUnitM.LaunchWizardForm, ToolsAPI, SBD.ServiceProvider
  , DUnitM.IDE_API, Dialogs;



function DesignTimeServices: IServiceProvider;
begin
result := StandardServiceProvider;
DUnitM.LaunchWizardForm.RegisterStockLaunchWizardService( result);
DUnitM.IDE_API.RegisterStockDevEnviroService( result)
end;

procedure Register;
var
  Services: IServiceProvider;
  IDE: IIDE_API;
begin
Services := DesignTimeServices;
if Services.Gn.Acquire<IIDE_API>( nil, IDE) then
    IDE.RegisterPackageWizard(
      DUnitM.Wizard.TProjectWizard.Create( Services))
  else
    ShowMessage('IDE services not found.');
end;

end.
