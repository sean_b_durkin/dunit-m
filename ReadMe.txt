{***************************************************************************}
{                                                                           }
{        DUnit-M                                                            }
{                                                                           }
{        Copyright (C) 2015 Sean B. Durkin                                  }
{                                                                           }
{        Author: Sean B. Durkin                                             }
{        sean@seanbdurkin.id.au                                             }
{                                                                           }
{                                                                           }
{***************************************************************************}
{                                                                           }
{  Licensed under the Apache License, Version 2.0 (the "License");          }
{  you may not use this file except in compliance with the License.         }
{  You may obtain a copy of the License at                                  }
{                                                                           }
{      http://www.apache.org/licenses/LICENSE-2.0                           }
{                                                                           }
{  Unless required by applicable law or agreed to in writing, software      }
{  distributed under the License is distributed on an "AS IS" BASIS,        }
{  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. }
{  See the License for the specific language governing permissions and      }
{  limitations under the License.                                           }
{                                                                           }
{***************************************************************************}

{***************************************************************************}
{                                                                           }
{               NOTICE of DERIVATION and CHANGE                             }
{                                                                           }
{  This project is a derived work. It is dervied from the DUnitX library    }
{  created By Vincent Parrett                                               }
{  (hosted at https://github.com/VSoftTechnologies/DUnitX).                 }
{  The copyright holder for the original code is as per the following       }
{  comment block. The copyright holder for all changes in this file from    }
{  that base is as denoted following.                                       }
{                                                                           }
{        Copyright (C) 2015 Sean B. Durkin                                  }
{                                                                           }
{        Author: Sean B. Durkin                                             }
{        sean@seanbdurkin.id.au                                             }
{***************************************************************************}

// The Original DUnitX comment header was ....

{***************************************************************************}
{                                                                           }
{           DUnitX                                                          }
{                                                                           }
{           Copyright (C) 2012 Vincent Parrett                              }
{                                                                           }
{           vincent@finalbuilder.com                                        }
{           http://www.finalbuilder.com                                     }
{                                                                           }
{ Contributors : Vincent Parrett                                            }
{                Jason Smith                                                }
{                Nick Hodges                                                }
{                Nicholas Ring                                              }
{                                                                           }
{***************************************************************************}

DUnit-M is a unit testing framework for software written in and for Delphi XE7
or later. Some of the files from this project are either copied or derived
from the DUnitX project.

The focus and key selling feature of DUnit-M is data-driven testing.

Features include:
* Data-driven testing support
* Multiple loggers are configurable through an observer-subscriber pattern
* An application generation wizard to assist in the creation of unit testing
   programs.

INSTALLATION
=============
1. Environment options
In the Delphi Environment options, add the following two paths to the Library path,
  for each Platform ...

<#DUnit-m>\ephemeral\dcp\<#compiler>\$(Platform)\$(Config)
<#DUnit-m>\ephemeral\dcu\<#compiler>\$(Platform)\$(Config)

where:
  <#DUnit-m>   is a place-marker for the directory that you installed DUnit-m into.
               For example, it contains this read-me file. Replace with its value.
  <#compiler>  is a place-marker for the compiler short moniker.
               For example 'XE7' for Delphi XE7. Replace with its value.
  $(Platform)  Leave this in as a literal. Do not replace.
  $(Config)    Leave this in as a literal. Do not replace.

2. Open the project group
Open the project group in your Delphi IDE. The project group will be locate at ...

  <#DUnit-m>\package-heads\<#compiler>\DUnitM_Group.groupproj

If there are no project heads for your particular version of Delphi ...
BEGIN
  Simply copy the dpk's and open them up in the IDE and open the project options.

  Set the project options how you please. For example the supplied projects use options ...

    (For Config='All configurations', and of course replace <#compiler> with something like 'XE7'.)
    DCP output directory = ..\..\ephemeral\dcp\<#compiler>\$(Platform)\$(Config)
    Unit output directory = ..\..\ephemeral\dcu\<#compiler>\$(Platform)\$(Config)

  Mark the runtime package as runtime and the designtime package as designtime.
  Save your new projects and group together in a project group.

  In unit DUnitM.IDE_API, in the following two functions, if you compiler is
  not represented in the implementations of these functions, add them in.

    function ShortCompilerName(): string;
    function GetRegistryBase(): string;

END

3. Compile
Compile the runtime package and install the designtime package.

4. Usage
After installation, you can access the project wizard via IDE menu path ...

 File |  New | Other... | Unit Test | DUnitM Project Wizard

Use the project wizard to create unit testing applications. Of course the
 wizard will not magically write your specific unit tests. You have to do that.
